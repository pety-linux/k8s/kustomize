# kustomize
** kustomization.yaml
```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
- nginx-depl.yaml
- nginx-service.yaml
```

** Managing dirs
```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
- api/api-depl.yaml
- api/api-service.yaml
- db/db-depl.yaml
- db/db-service.yaml
```

** Create stack
```
kustomize build k8s/ | kubectl apply –f -
```
** Delete stack
```
kustomize build k8s/ | kubectl delete –f 
kubectl delete –k k8s/
```
